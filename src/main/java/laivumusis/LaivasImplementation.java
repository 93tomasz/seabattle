package laivumusis;

public class LaivasImplementation implements Laivas {
    private int dydis;

    public LaivasImplementation(int dydis) {
        this.dydis = dydis;
    }

    public int gautiX() {
        return 0;
    }

    public int gautiY() {
        return 0;
    }

    public int gautiDydi() {
        return dydis;
    }
}
