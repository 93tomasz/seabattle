package laivumusis;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AplinkaHibernateImplementation implements Aplinka {
    private int laivuSkaicius;
    private EntityManagerFactory sessionFactory;

    public AplinkaHibernateImplementation() throws Exception {
        setUp();
    }

    public Laivas padetiLaiva(int dydis, int x, int y, Kryptis kryptis) {
        LaivasImplementation naujasLaivas = new LaivasImplementation(4);

        laivuSkaicius ++;

        return naujasLaivas;
    }

    public int gautiLaivuSkaiciu() {
        return laivuSkaicius;
    }

    public int gautiGyvuLaivuSkaiciu() {
        return 0;
    }

    public Laivas[][] gautiLenta() {
        return new Laivas[0][];
    }

    public boolean sauti(int x, int y) {
        return false;
    }


    protected void setUp() throws Exception {
        sessionFactory = Persistence.createEntityManagerFactory( "org.hibernate.tutorial.jpa" );
    }
}
