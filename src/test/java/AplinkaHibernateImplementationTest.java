import laivumusis.AplinkaHibernateImplementation;
import laivumusis.Kryptis;
import laivumusis.Laivas;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AplinkaHibernateImplementationTest {
    private AplinkaHibernateImplementation aplinkaGlobal;

    @Before
    public void pradzia() throws Exception {
        aplinkaGlobal = new AplinkaHibernateImplementation();
    }

    @Test
    public void tikrinamArPradziojeLaivuYraNulis() throws Exception {
//        padetiLaiva(1, 1, 2);
        AplinkaHibernateImplementation aplinka = new AplinkaHibernateImplementation();

        Integer kiekis = aplinka.gautiLaivuSkaiciu();

        Assert.assertTrue(kiekis == 0);
    }

    @Test
    public void tikrinamArPoPadejimoLaivuYraVienas() throws Exception {
        AplinkaHibernateImplementation aplinka = new AplinkaHibernateImplementation();

        aplinka.padetiLaiva(0, 0 ,0, Kryptis.HORIZONTAL);

        Integer kiekis = aplinka.gautiLaivuSkaiciu();

        Assert.assertTrue(kiekis == 1);
    }

    @Test
    public void tikrinamKokiLaivaPadejom() {
        Laivas pirmasLaivas = aplinkaGlobal.padetiLaiva(0, 0 ,0, Kryptis.HORIZONTAL);

        Assert.assertTrue(pirmasLaivas.gautiDydi() > 0);
    }
//
//    @Test
//    public void tikrintiArLaivuiPriskyreXirYKoordinates() {
//        LaivasImplementation pirmasLaivas = aplinkaGlobal.padetiLaiva(4, 2, 2);
//
//        Assert.assertTrue(pirmasLaivas.gautiX() == 2);
//        Assert.assertTrue(pirmasLaivas.gautiY() == 2);
//    }

//    @Test
//    public void tikrintiArPaduodamosKoordinatesNeuzeinaUzLentosRibu() {
//        LaivasImplementation pirmasLaivas = aplinkaGlobal.padetiLaiva(4, 11, 11);
//        LaivasImplementation antrasLaivas = aplinkaGlobal.padetiLaiva(4, 4, 4);
//
//        Assert.assertTrue(pirmasLaivas == null);
//        Assert.assertTrue(antrasLaivas != null);
//    }
}

